//
// Bài 1: Quản lý tuyển sinh
// -----------------------------------------
function tinhKetQuaThi() {
  // Lấy điểm đối tượng
  var diemDoiTuong = 0;

  if (document.getElementById("doiTuong1").checked == true) {
    diemDoiTuong = 2.5;
  } else if (document.getElementById("doiTuong2").checked == true) {
    diemDoiTuong = 1.5;
  } else if (document.getElementById("doiTuong3").checked == true) {
    diemDoiTuong = 1;
  } else {
    diemDoiTuong = 0;
  }
  //   console.log("điểm đối tượng", diemDoiTuong); // kiểm tra giá trị trả về

  //   lấy điểm khu vực
  var diemKhuVuc = 0;

  if (document.getElementById("txt-khu-vuc").value == "A") {
    diemKhuVuc = 2;
  } else if (document.getElementById("txt-khu-vuc").value == "B") {
    diemKhuVuc = 1;
  } else if (document.getElementById("txt-khu-vuc").value == "C") {
    diemKhuVuc = 0.5;
  } else {
    diemKhuVuc = 0;
  }
  //   console.log("diemKhuVuc:", diemKhuVuc); // kiểm tra giá trị trả về

  // so sánh điểm chuẩn
  var diemChuan = document.getElementById("txt-diemChuan").value * 1;
  var diemMon1 = document.getElementById("txt-diemMon1").value * 1;
  var diemMon2 = document.getElementById("txt-diemMon2").value * 1;
  var diemMon3 = document.getElementById("txt-diemMon3").value * 1;
  var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemDoiTuong + diemKhuVuc;

  //   Có 1 trong 3 điểm là 0 thì rớt
  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    document.getElementById("txt-result").innerHTML = `
    Bạn đã rớt <br>
    Tổng điểm của bạn là: ${tongDiem}
    `;
  } else if (tongDiem >= diemChuan) {
    // so sánh với điểm chuẩn
    document.getElementById("txt-result").innerHTML = `
    Bạn đã đậu <br>
    Tổng điểm của bạn là: ${tongDiem}
    `;
  } else {
    document.getElementById("txt-result").innerHTML = `
    Bạn đã rớt <br>
    Tổng điểm của bạn là: ${tongDiem}`;
  }
}

//
// Bài 2: Tính tiền điện
// -----------------------------------------
function tinhTienDien() {
  var hoTen = document.getElementById("hoTen").value;
  var soKW = document.getElementById("so-kw").value * 1;
  var tongTienDien = 0;

  if (soKW <= 50) {
    tongTienDien = soKW * 500;
  } else if (soKW > 50 && soKW <= 100) {
    tongTienDien = 50 * 500 + (soKW - 50) * 650;
  } else if (soKW > 100 && soKW <= 200) {
    tongTienDien = 50 * 500 + 50 * 650 + (soKW - 100) * 850;
  } else if (soKW > 200 && soKW <= 350) {
    tongTienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKW - 200) * 1100;
  } else {
    tongTienDien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKW - 350) * 1300;
  }

  document.getElementById("soTienDien").innerHTML = `
  Họ tên: ${hoTen} <br>
  Giá tiền: ${tongTienDien.toLocaleString()} VNĐ
  `;
}

//
// Bài 3: Tính thuế thu nhập cá nhân
// -----------------------------------------
function tinhTienThue() {
  var hoTen = document.getElementById("ho-ten").value;
  var tongThuNhap = document.getElementById("tong-thu-nhap").value * 1;
  var soNguoiPhuThuoc = document.getElementById("so-nguoi-phu-thuoc").value * 1;
  var thueTNCN = tongThuNhap - 4e6 - soNguoiPhuThuoc * 16e5;
  var tienThue = 0;

  if (tongThuNhap > 0 && tongThuNhap <= 6e7) {
    tienThue = 0.05 * thueTNCN;
  } else if (tongThuNhap > 6e7 && tongThuNhap <= 12e7) {
    tienThue = 0.1 * thueTNCN;
  } else if (tongThuNhap > 12e7 && tongThuNhap <= 21e7) {
    tienThue = 0.15 * thueTNCN;
  } else if (tongThuNhap > 21e7 && tongThuNhap <= 384e6) {
    tienThue = 0.2 * thueTNCN;
  } else if (tongThuNhap > 384e6 && tongThuNhap <= 624e6) {
    tienThue = 0.25 * thueTNCN;
  } else if (tongThuNhap > 624e6 && tongThuNhap <= 96e7) {
    tienThue = 0.3 * thueTNCN;
  } else {
    tienThue = 0.35 * thueTNCN;
  }

  document.getElementById("soTienThue").innerHTML = `
  Họ tên: ${hoTen} <br>
  Số tiền thuế: ${tienThue.toLocaleString()} VNĐ
  `;
}

//
// Bài 4: Tính tiền cáp
// -----------------------------------------
function tinhTienCap() {
  var maKhachHang = document.getElementById("ma-khach-hang").value;
  var loaiKhachHang = document.getElementById("loai-khach-hang").value;
  var soKenh = document.getElementById("so-kenh").value * 1;
  var soKetNoi = document.getElementById("so-ket-noi").value * 1;
  var phiHoaDon, phiDichVu, phiThueKenh;
  var tienCap = 0;

  if (loaiKhachHang.value === "caNhan") {
    phiHoaDon = 4.5;
    phiDichVu = 20.5;
    phiThueKenh = 7.5 * soKenh;
    tienCap = phiDichVu + phiHoaDon + phiThueKenh;
  } else if (loaiKhachHang.value === "doanhNghiep") {
    phiHoaDon = 15;
    phiDichVu = 75;
    phiThueKenh = 50 * soKenh;
    tienCap = phiHoaDon + phiDichVu + phiThueKenh;
    if (soKetNoi > 10) {
      tienCap += (soKetNoi - 10) * 5;
    }
  }

  document.getElementById("soTienCap").innerHTML = `
  Mã khách hàng: ${maKhachHang} <br />
  Số tiền cáp: ${tienCap}
  `;
}
